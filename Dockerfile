FROM ubuntu:17.10

MAINTAINER agilob <dockerhub@agilob.net>

RUN apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get install -y make qt5-qmake qt5-default build-essential qtdeclarative5-dev clang g++ --no-install-recommends && \
    apt-get -y autoremove && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/*
